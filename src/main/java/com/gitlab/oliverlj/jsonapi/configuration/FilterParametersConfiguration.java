package com.gitlab.oliverlj.jsonapi.configuration;

import java.util.List;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import com.gitlab.oliverlj.jsonapi.configuration.parameters.RequestParamFilterParametersMethodArgumentResolver;

@Configuration(proxyBeanMethods = false)
public class FilterParametersConfiguration implements WebMvcConfigurer {

  @Override
  public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
    argumentResolvers.add(new RequestParamFilterParametersMethodArgumentResolver());
  }

}
