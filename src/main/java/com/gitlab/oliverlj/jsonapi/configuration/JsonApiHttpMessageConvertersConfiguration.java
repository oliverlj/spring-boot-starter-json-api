package com.gitlab.oliverlj.jsonapi.configuration;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.jasminb.jsonapi.ResourceConverter;

/**
 * Configuration for HTTP Message converters that use json api.
 * 
 * @author Olivier LE JACQUES (o.le.jacques@gmail.com)
 *
 */
@Configuration(proxyBeanMethods = false)
@ConditionalOnBean(ObjectMapper.class)
@ConditionalOnClass(ResourceConverter.class)
@ConditionalOnProperty(name = JsonApiHttpMessageConvertersConfiguration.PREFERRED_MAPPER_PROPERTY, havingValue = "json-api")
@EnableConfigurationProperties(SpringBootStaterJsonApiProperties.class)
@Import({JsonApiResponseEntityExceptionHandler.class, FilterParametersConfiguration.class})
public class JsonApiHttpMessageConvertersConfiguration {

  static final String PREFERRED_MAPPER_PROPERTY = "spring.http.converters.preferred-json-mapper";

  @Bean
  public HttpMessageConverters jsonApiHttpMessageConverter(ObjectMapper mapper, ResourceConverterWrapper wrapper) {
    return new HttpMessageConverters(new JsonApiHttpMessageConverter(mapper, wrapper));
  }

  @Bean
  public ResourceConverterWrapper resourceConverterWrapper(ObjectMapper mapper, SpringBootStaterJsonApiProperties properties) {
    return new ResourceConverterWrapper(mapper, properties);
  }
}
