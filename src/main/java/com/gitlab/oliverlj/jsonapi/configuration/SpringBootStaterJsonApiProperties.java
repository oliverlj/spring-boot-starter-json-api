package com.gitlab.oliverlj.jsonapi.configuration;

import static java.util.Collections.emptyList;
import java.util.List;
import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Getter;
import lombok.Setter;

/**
 * Spring boot stater json api properties.
 * 
 * @author Olivier LE JACQUES (o.le.jacques@gmail.com)
 *
 */
@ConfigurationProperties(prefix = "spring.json-api", ignoreUnknownFields = false)
@Getter
@Setter
public class SpringBootStaterJsonApiProperties {

  /**
   * Json api converter deserialization options to disable.
   */
  private List<String> disableDeserializationOptions = emptyList();
  /**
   * Json api converter deserialization options to enable.
   */
  private List<String> enableDeserializationOptions = emptyList();
  /**
   * Json api converter serialization options to disable.
   */
  private List<String> disableSerializationOptions = emptyList();
  /**
   * Json api converter serialization options to disable.
   */
  private List<String> enableSerializationOptions = emptyList();
}
