package com.gitlab.oliverlj.jsonapi.configuration.parameters;

/**
 * The filter operator (equal, not equal, ...) of the value to use.
 * 
 * @author Olivier LE JACQUES (o.le.jacques@gmail.com)
 *
 */
public enum FilterOperator {

  /**
   * Equal.
   */
  EQ,
  /**
   * Like.
   */
  LIKE

}
