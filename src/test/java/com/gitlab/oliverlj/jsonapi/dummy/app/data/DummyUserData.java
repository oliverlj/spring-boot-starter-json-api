package com.gitlab.oliverlj.jsonapi.dummy.app.data;

import org.springframework.lang.Nullable;
import com.github.jasminb.jsonapi.LongIdHandler;
import com.github.jasminb.jsonapi.annotations.Id;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Type("users")
public class DummyUserData {

  @Id(LongIdHandler.class)
  @Nullable
  private Long id;
}
