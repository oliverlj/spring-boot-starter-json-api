package com.gitlab.oliverlj.jsonapi.dummy.app.controllers;

import java.util.Arrays;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import com.gitlab.oliverlj.jsonapi.dummy.app.data.DummyUserData;

@RestController
@RequestMapping(DummyUserController.RESOURCE)
public class DummyUserController {

  public static final String RESOURCE = "users";
  public static final String REQUEST_1 = "request1";
  public static final String REQUEST_2 = "request2";
  public static final String RESOURCE_NOT_FOUND_ID = "1";

  @PostMapping(REQUEST_1)
  public DummyUserData userRequest(@RequestBody DummyUserData userDataTest) {
    return userDataTest;
  }

  @GetMapping(RESOURCE_NOT_FOUND_ID)
  public void userNotFound() {
    throw new ResponseStatusException(HttpStatus.NOT_FOUND);
  }

  @GetMapping(REQUEST_2)
  public Page<DummyUserData> pageOfUsers() {
    DummyUserData dummyUserData = new DummyUserData();
    dummyUserData.setId(1L);
    return new PageImpl<>(Arrays.asList(dummyUserData));
  }
}
