package com.gitlab.oliverlj.jsonapi.configuration;

import static com.gitlab.oliverlj.jsonapi.configuration.JsonApiHttpMessageConverter.APPLICATION_JSON_API;
import static org.assertj.core.api.Assertions.assertThat;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Collection;
import org.apache.commons.lang3.reflect.TypeUtils;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.oliverlj.jsonapi.dummy.app.controllers.DummyUserController;
import com.gitlab.oliverlj.jsonapi.dummy.app.data.DummyUserData;

/**
 * Unit test of {@link JsonApiHttpMessageConverter}.
 * 
 * @author Olivier LE JACQUES (o.le.jacques@gmail.com)
 *
 */
public class JsonApiHttpMessageConverterUnitTest {

  @Test
  public void canReadUserDataTest() {
    // Given
    ObjectMapper objectMapper = new ObjectMapper();
    JsonApiHttpMessageConverter jsonApiHttpMessageConverter =
        new JsonApiHttpMessageConverter(objectMapper, new ResourceConverterWrapper(objectMapper, new SpringBootStaterJsonApiProperties()));
    Class<?> type = DummyUserData.class;
    Class<?> contextClass = DummyUserController.class;

    // When
    boolean canRead = jsonApiHttpMessageConverter.canRead(type, contextClass, null);

    // Then
    assertThat(canRead).isTrue();
  }

  @Test
  public void canReadIterableOfUserDataTests() {
    // Given
    ObjectMapper objectMapper = new ObjectMapper();
    JsonApiHttpMessageConverter jsonApiHttpMessageConverter =
        new JsonApiHttpMessageConverter(objectMapper, new ResourceConverterWrapper(objectMapper, new SpringBootStaterJsonApiProperties()));
    ParameterizedType type = TypeUtils.parameterize(Iterable.class, DummyUserData.class);
    Class<?> contextClass = ArrayList.class;

    // When
    boolean canRead = jsonApiHttpMessageConverter.canRead(type, contextClass, null);

    // Then
    assertThat(canRead).isTrue();
  }

  @Test
  public void canReadCollectionOfUserDataTests() {
    // Given
    ObjectMapper objectMapper = new ObjectMapper();
    JsonApiHttpMessageConverter jsonApiHttpMessageConverter =
        new JsonApiHttpMessageConverter(objectMapper, new ResourceConverterWrapper(objectMapper, new SpringBootStaterJsonApiProperties()));
    ParameterizedType type = TypeUtils.parameterize(Collection.class, DummyUserData.class);
    Class<?> contextClass = ArrayList.class;

    // When
    boolean canRead = jsonApiHttpMessageConverter.canRead(type, contextClass, null);

    // Then
    assertThat(canRead).isTrue();
  }

  @Test
  public void canReadJsonApiMediaType() {
    // Given
    ObjectMapper objectMapper = new ObjectMapper();
    JsonApiHttpMessageConverter jsonApiHttpMessageConverter =
        new JsonApiHttpMessageConverter(objectMapper, new ResourceConverterWrapper(objectMapper, new SpringBootStaterJsonApiProperties()));
    Class<?> clazz = DummyUserData.class;
    MediaType jsonApiMediaType = APPLICATION_JSON_API;

    // When
    boolean canRead = jsonApiHttpMessageConverter.canRead(clazz, clazz, jsonApiMediaType);

    // Then
    assertThat(canRead).isTrue();
  }

  @Test
  public void cannotReadObject() {
    // Given
    ObjectMapper objectMapper = new ObjectMapper();
    JsonApiHttpMessageConverter jsonApiHttpMessageConverter =
        new JsonApiHttpMessageConverter(objectMapper, new ResourceConverterWrapper(objectMapper, new SpringBootStaterJsonApiProperties()));
    Class<?> type = Object.class;
    Class<?> contextClass = DummyUserController.class;

    // When
    boolean canRead = jsonApiHttpMessageConverter.canRead(type, contextClass, null);

    // Then
    assertThat(canRead).isFalse();
  }

  @Test
  public void cannotReadIterableOfObjects() {
    // Given
    ObjectMapper objectMapper = new ObjectMapper();
    JsonApiHttpMessageConverter jsonApiHttpMessageConverter =
        new JsonApiHttpMessageConverter(objectMapper, new ResourceConverterWrapper(objectMapper, new SpringBootStaterJsonApiProperties()));
    ParameterizedType type = TypeUtils.parameterize(Iterable.class, Object.class);
    Class<?> contextClass = ArrayList.class;

    // When
    boolean canRead = jsonApiHttpMessageConverter.canRead(type, contextClass, null);

    // Then
    assertThat(canRead).isFalse();
  }

  @Test
  public void cannotReadCollectionOfObjects() {
    // Given
    ObjectMapper objectMapper = new ObjectMapper();
    JsonApiHttpMessageConverter jsonApiHttpMessageConverter =
        new JsonApiHttpMessageConverter(objectMapper, new ResourceConverterWrapper(objectMapper, new SpringBootStaterJsonApiProperties()));
    ParameterizedType type = TypeUtils.parameterize(Collection.class, Object.class);
    Class<?> contextClass = ArrayList.class;

    // When
    boolean canRead = jsonApiHttpMessageConverter.canRead(type, contextClass, null);

    // Then
    assertThat(canRead).isFalse();
  }

  @Test
  public void cannotReadJsonMediaType() {
    // Given
    ObjectMapper objectMapper = new ObjectMapper();
    JsonApiHttpMessageConverter jsonApiHttpMessageConverter =
        new JsonApiHttpMessageConverter(objectMapper, new ResourceConverterWrapper(objectMapper, new SpringBootStaterJsonApiProperties()));
    Class<?> clazz = DummyUserData.class;
    MediaType jsonMediaType = MediaType.APPLICATION_JSON;

    // When
    boolean canRead = jsonApiHttpMessageConverter.canRead(clazz, clazz, jsonMediaType);

    // Then
    assertThat(canRead).isFalse();
  }

  @Test
  public void canWriteUserDataTest() {
    // Given
    ObjectMapper objectMapper = new ObjectMapper();
    JsonApiHttpMessageConverter jsonApiHttpMessageConverter =
        new JsonApiHttpMessageConverter(objectMapper, new ResourceConverterWrapper(objectMapper, new SpringBootStaterJsonApiProperties()));
    Class<?> clazz = DummyUserData.class;

    // When
    boolean canWrite = jsonApiHttpMessageConverter.canWrite(clazz, clazz, null);

    // Then
    assertThat(canWrite).isTrue();
  }

  @Test
  public void canWriteIterableOfUserDataTests() {
    // Given
    ObjectMapper objectMapper = new ObjectMapper();
    JsonApiHttpMessageConverter jsonApiHttpMessageConverter =
        new JsonApiHttpMessageConverter(objectMapper, new ResourceConverterWrapper(objectMapper, new SpringBootStaterJsonApiProperties()));
    ParameterizedType type = TypeUtils.parameterize(Iterable.class, DummyUserData.class);
    Class<?> contextClass = ArrayList.class;

    // When
    boolean canWrite = jsonApiHttpMessageConverter.canWrite(type, contextClass, null);

    // Then
    assertThat(canWrite).isTrue();
  }

  @Test
  public void canWriteCollectionOfUserDataTests() {
    // Given
    ObjectMapper objectMapper = new ObjectMapper();
    JsonApiHttpMessageConverter jsonApiHttpMessageConverter =
        new JsonApiHttpMessageConverter(objectMapper, new ResourceConverterWrapper(objectMapper, new SpringBootStaterJsonApiProperties()));
    ParameterizedType type = TypeUtils.parameterize(Collection.class, DummyUserData.class);
    Class<?> contextClass = ArrayList.class;

    // When
    boolean canWrite = jsonApiHttpMessageConverter.canWrite(type, contextClass, null);

    // Then
    assertThat(canWrite).isTrue();
  }

  @Test
  public void canWritePageOfUserDataTests() {
    // Given
    ObjectMapper objectMapper = new ObjectMapper();
    JsonApiHttpMessageConverter jsonApiHttpMessageConverter =
        new JsonApiHttpMessageConverter(objectMapper, new ResourceConverterWrapper(objectMapper, new SpringBootStaterJsonApiProperties()));
    ParameterizedType type = TypeUtils.parameterize(Page.class, DummyUserData.class);
    Class<?> contextClass = PageImpl.class;

    // When
    boolean canWrite = jsonApiHttpMessageConverter.canWrite(type, contextClass, null);

    // Then
    assertThat(canWrite).isTrue();
  }

  public void canWriteJsonApiMediaType() {
    // Given
    ObjectMapper objectMapper = new ObjectMapper();
    JsonApiHttpMessageConverter jsonApiHttpMessageConverter =
        new JsonApiHttpMessageConverter(objectMapper, new ResourceConverterWrapper(objectMapper, new SpringBootStaterJsonApiProperties()));
    Class<?> clazz = DummyUserData.class;
    MediaType jsonApiMediaType = APPLICATION_JSON_API;

    // When
    boolean canWrite = jsonApiHttpMessageConverter.canWrite(clazz, clazz, jsonApiMediaType);

    // Then
    assertThat(canWrite).isTrue();
  }

  @Test
  public void cannotWriteObject() {
    // Given
    ObjectMapper objectMapper = new ObjectMapper();
    JsonApiHttpMessageConverter jsonApiHttpMessageConverter =
        new JsonApiHttpMessageConverter(objectMapper, new ResourceConverterWrapper(objectMapper, new SpringBootStaterJsonApiProperties()));
    Class<?> clazz = Object.class;

    // When
    boolean canWrite = jsonApiHttpMessageConverter.canWrite(clazz, clazz, null);

    // Then
    assertThat(canWrite).isFalse();
  }

  @Test
  public void cannotWriteIterableOfObjects() {
    // Given
    ObjectMapper objectMapper = new ObjectMapper();
    JsonApiHttpMessageConverter jsonApiHttpMessageConverter =
        new JsonApiHttpMessageConverter(objectMapper, new ResourceConverterWrapper(objectMapper, new SpringBootStaterJsonApiProperties()));
    ParameterizedType type = TypeUtils.parameterize(Iterable.class, Object.class);
    Class<?> contextClass = ArrayList.class;

    // When
    boolean canWrite = jsonApiHttpMessageConverter.canWrite(type, contextClass, null);

    // Then
    assertThat(canWrite).isFalse();
  }

  @Test
  public void cannotWriteCollectionOfObjects() {
    // Given
    ObjectMapper objectMapper = new ObjectMapper();
    JsonApiHttpMessageConverter jsonApiHttpMessageConverter =
        new JsonApiHttpMessageConverter(objectMapper, new ResourceConverterWrapper(objectMapper, new SpringBootStaterJsonApiProperties()));
    ParameterizedType type = TypeUtils.parameterize(Collection.class, Object.class);
    Class<?> contextClass = ArrayList.class;

    // When
    boolean canWrite = jsonApiHttpMessageConverter.canWrite(type, contextClass, null);

    // Then
    assertThat(canWrite).isFalse();
  }

  @Test
  public void cannotWriteJsonMediaType() {
    // Given
    ObjectMapper objectMapper = new ObjectMapper();
    JsonApiHttpMessageConverter jsonApiHttpMessageConverter =
        new JsonApiHttpMessageConverter(objectMapper, new ResourceConverterWrapper(objectMapper, new SpringBootStaterJsonApiProperties()));
    Class<?> clazz = DummyUserData.class;
    MediaType jsonMediaType = MediaType.APPLICATION_JSON;

    // When
    boolean canWrite = jsonApiHttpMessageConverter.canWrite(clazz, clazz, jsonMediaType);

    // Then
    assertThat(canWrite).isFalse();
  }
}
