# CHANGELOG

<!--- next entry here -->

## 0.4.0
2020-04-12

### Features

- expose resource converter as a ResourceConverterWrapper bean (d07a9f61c83e2a5c6b023e44fd886afb5d9cabd4)

## 0.3.0
2020-03-29

### Features

- **pagination:** return meta.totalPages on Page object (44268ffc49911ea97d27d46f60952f8bfe83ddf3)

### Fixes

- inject spring object mapper for jackon configuration (8d79b353b46e7801c92248b8bd3d8a267d01ae5b)

## 0.2.0
2020-01-03

### Features

- add support for deserialization / serialization features (0a794e6d55f62e0973e69116d35962a6dbc5bdf5)

### Fixes

- replace ResourceNotFoundException by ResponseStatusException (7b229d9fc74d3ed9fde15e815bc4753163840532)
- rename spring-boot-starter-json-api by spring.json-api (0d9f318738f0e928dbb0e5403eccf978acb9145d)

## 0.1.0
2019-12-13

### Features

- initial commit (4c5af84f9db67ab28c8a967c55d285bdc8855bac)
- replace ST operator by like (c88448447a2609979a13c5a5b884b7d4bd227ce8)
- add resource not found exception (64f31ac2f4ee2ea2f67d9b81714b86d0926a91bb)

### Fixes

- add licence (71b8a02f43c504e62e85b995ccc797bfccb3fd26)
- add readme.md (44362d575ac2ab0c69b5672c002c18a9ce0dde5c)
- support iterable and collection as request and return parameters (e74d9415203edd69938cfe4adf6a1d0b37ba72d1)
- can not read simple type request body (d73d04f03c5cf7f3e21b45bf4c667d5e19eafa15)
- add non null eclipse api (6740e7ba9d07f60f9d91c4cfd5c7cf280d094bd6)
- move the code base to google styleguide (f67e713b1bd08a03ed3330b61e61500ed1088a33)
- reorganize dummy app (7713fe7268792ad0e3be988055cb7f27c7f5af63)