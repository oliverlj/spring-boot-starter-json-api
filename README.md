### spring-boot-starter-json-api

This spring boot starter brings json api converter (https://github.com/jasminb/jsonapi-converter) for your great spring project.

JSONAPI-Converter is a library that provides means for integrating with services using JSON API specification.

For information on JSON API specification please see: http://jsonapi.org/format/

#### Including the library in your project

Maven:

Add dependency from maven central

```xml
<dependency>
	<groupId>com.gitlab.oliverlj</groupId>
	<artifactId>spring-boot-starter-json-api</artifactId>
	<version>0.4.0</version>
</dependency>
```

#### Activate the json-api

Use the property :

```yaml
spring.http.converters.preferred-json-mapper: json-api
```

#### Activate / Desactivate deserialization / serialization features of json api

Use the property :

```yaml
spring.json-api:
  enable-serialization-options: INCLUDE_RELATIONSHIP_ATTRIBUTES
  disable-deserialization-options: REQUIRE_RESOURCE_ID
```